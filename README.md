## Proyecto api Laravel 9

### Herramientas usadas

- xampp
    - php 8
    - phpmyadmin
    - postman

### Ejecutar los siguientes comandos 
```
cd laravel9-exercise
composer install
php artisan serve

```

### Para visualizar el proyecto accedemos a 

http://localhost:8000/


### Base de datos

 * crear la base de datos laravel9-exercise
 * php artisan migrate
 * php artisan db:seed


### Agregar el archivo .env en la raiz del proyecto con el siguiente contenido

```
APP_NAME=Laravel
APP_ENV=local
APP_KEY=base64:4qt0GM90VCwcrpH+Yc8zfV8L19dtakAlZHEnz2h0BLI=
APP_DEBUG=true
APP_URL=http://localhost

LOG_CHANNEL=stack
LOG_DEPRECATIONS_CHANNEL=null
LOG_LEVEL=debug

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=laravel9-exercise
DB_USERNAME=root
DB_PASSWORD=

BROADCAST_DRIVER=log
CACHE_DRIVER=file
FILESYSTEM_DISK=local
QUEUE_CONNECTION=sync
SESSION_DRIVER=file
SESSION_LIFETIME=120

MEMCACHED_HOST=127.0.0.1

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

MAIL_MAILER=smtp
MAIL_HOST=mailhog
MAIL_PORT=1025
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null
MAIL_FROM_ADDRESS="hello@example.com"
MAIL_FROM_NAME="${APP_NAME}"

AWS_ACCESS_KEY_ID=
AWS_SECRET_ACCESS_KEY=
AWS_DEFAULT_REGION=us-east-1
AWS_BUCKET=
AWS_USE_PATH_STYLE_ENDPOINT=false

PUSHER_APP_ID=
PUSHER_APP_KEY=
PUSHER_APP_SECRET=
PUSHER_HOST=
PUSHER_PORT=443
PUSHER_SCHEME=https
PUSHER_APP_CLUSTER=mt1

VITE_PUSHER_APP_KEY="${PUSHER_APP_KEY}"
VITE_PUSHER_HOST="${PUSHER_HOST}"
VITE_PUSHER_PORT="${PUSHER_PORT}"
VITE_PUSHER_SCHEME="${PUSHER_SCHEME}"
VITE_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"

```


### Postman

En la raíz del proyecto se encuentra el archivo  ´Laravel9-exercise.postman_collection´ , es la collección de postman que se puede usar para probar la api una vez completados los pasos anteriores.



### Nota

No se consideró usar sail porque instalaba cosas que no son necesarias, aparte de en un principio tener un inconveniente con la version 7.4 de php.

Se probó usar un contenedor de Laravel9 de bitnami (también dio error)