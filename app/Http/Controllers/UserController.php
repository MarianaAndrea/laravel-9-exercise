<?php

namespace App\Http\Controllers;

use App\Http\Requests\SaveUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {
        return User::all();
    }
    public function store(Request $request)
    {
        User::create($request->all());
        return response()->json([
            'res' => true,
            'msg' => 'Usuario guardado correctamente'
        ],200);
    }

    public function show( User $user)
    {
        return response()->json([
            'res' => true,
            'user' => $user
        ],200);
    }

    public function update(UpdateUserRequest $request, User $user)
    {
        $user->update($request->all());
        return response()->json([
            'res'=>true,
            'mensaje'=>'Update User success'
        ],200);

    }

    public function destroy(User $user)
    {
        $user->delete();
        return response()->json([
                    'res'=>true,
                    'mensaje'=>'Delete user success'
                ],200);
    }
}

